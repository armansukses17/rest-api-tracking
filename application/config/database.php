<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$json_config = file_get_contents('config.json');
$ambil_config = json_decode($json_config);
$active_group = 'r_'.$ambil_config->db[0]->config_name;
$query_builder = TRUE;
// print_r($ambil_config->db[0]->config_name);die;
foreach($ambil_config->db as $dbs){
	$db['r_'.$dbs->config_name] = array(
		'dsn'	=> '',
		'hostname' => $dbs->read->host,
		'username' => $dbs->read->user,
		'password' => $dbs->read->pass,
		'database' => $dbs->read->dbnm,
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE, // before TRUE
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt'  => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);

	$db['w_'.$dbs->config_name] = array(
		'dsn'	=> '',
		'hostname' => $dbs->write->host,
		'username' => $dbs->write->user,
		'password' => $dbs->write->pass,
		'database' => $dbs->write->dbnm,
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE, // before TRUE
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt'  => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
}
