<?php
// ------------------------------------------
// REST SERVER TRACKING RA
// Created: 06/01/2021
// Updated: -
// ------------------------------------------
//###########################################
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class RestServerRa extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() 
    {
        $this->response(['Not found !'], REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
    }

    // Search data by mawb
    function mawb_get() 
    {
        $string = $this->get('param'); // use param 
        // $string = $this->uri->segment(3); // by url (http://localhost/tracking/tracking-rest/RestTracking/mawb/8)
        if (!empty($string))
        {
            $this->db->select('id_, waybill_smu');
            $this->db->like('waybill_smu', $string);
            $this->db->where('_is_active', 1);
            $this->db->group_by('waybill_smu');
            $this->db->limit(10);
            $qryRegulated = $this->db->get('th_regulated');
            $checkData1 = $qryRegulated->num_rows();

            if ($checkData1 > 0) 
            {
                $result = $qryRegulated->result();
                foreach ($result as $row)
                {
                    $data[] = ['waybill_smu' => $row->waybill_smu];
                }
                $arr_result = ['s' => 'success', 'd' => $data];
                $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK            
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Not Found !', 'd' => []];
                $this->response($arr_result, REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
            }
        } 
        else 
        {
            $arr_result = ['s' => 'fail', 'm' => 'Bad request !', 'd' => []];
            $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        // echo json_encode($arr_result);
    }

    // after get and select mawb / waybill_smu, send this function by mawb / waybill_smu
    // get status by mawb (misal: th_inbound, th_outbond, atau th_regulated)
    // base_url()/status/99013695894
    function status_get() 
    {
        // $waybill_smu = $this->uri->segment(3);
        $waybill_smu = $this->get('mawb'); // use param 
        if (!empty($waybill_smu))
        {
            $this->db->select('*');
            $this->db->where(['waybill_smu' => $waybill_smu, '_is_active' => 1]);
            $query1 = $this->db->get('th_regulated');
            $checkRegulated = $query1->num_rows();
            $getInbond = $query1->row_array();
            // ------------------------------------------
            if ($checkRegulated > 0) 
            {
                $this->db->select('*');
                $this->db->where('proses_type', 'REGULATED');
                $this->db->order_by('_id', 'ASC');
                $queryStatus = $this->db->get('m_status_tracking');
                $checkStatus = $queryStatus->num_rows();
                $getData = $queryStatus->result();
                if ($checkStatus > 0) 
                {
                    foreach ($getData as $row) 
                    {
                        $data[] = [
                            'status_code' => $row->status_kode, 
                            'status_name' => $row->proses_type.' - '.$row->status_name
                        ];
                    }
                    $arr_result = ['s' => 'success', 'd' => $data];
                    $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK 
                }
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Not Found !'];
                $this->response($arr_result, REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
            }
        } 
        else 
        {
            $arr_result = ['s' => 'fail', 'm' => 'Bad request !'];
            $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
    }

    // post data 
    function create_post() 
    {
        // kirim parameter post yang dibutuhkan
        // ################################3###
        // $yourParam = [
        //     'mawb'          => 'xxxxx',
        //     'status_code'   => 'xxxxx',
        //     'date_time'     => 'YYYY-mm-dd H:I',
        //     'total_item'    => 'xxxxx', ---------> khusus regulated_rejected
        //     'desc_item'     => 'string' ---------> khusus regulated_rejected
        // ];
        // --------------------------------
        $mawb = $this->post('mawb');
        $status_code = $this->post('status_code');
        $date_time = $this->post('date_time'); // format : YYYY-mm-dd H:I
        $total_item = $this->post('total_item'); // ---------> khusus regulated_rejected
        $desc_item = $this->post('desc_item'); // ---------> khusus regulated_rejected

        // --------------------------------------
        if (empty($mawb) || empty($status_code) || empty($date_time))
        {
            $this->response(['s' => 'fail', 'm' => 'Enter your parameters correctly !'], REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        elseif (strlen($date_time) != 16) // jml string YYYY-mm-dd H:I
        {
            $this->response(['s' => 'fail', 'm' => 'Wrong date fornmat !'], REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        else
        {
            $explodeDate = explode(' ', $date_time);
            $status_date = $explodeDate[0]; // date
            $status_time = $explodeDate[1]; // time

            $tableHeader = 'th_regulated';

            $this->db->select('id_, waybill_smu');
            $this->db->where('waybill_smu', $mawb);
            $qryHdr = $this->db->get($tableHeader);
            $checkData = $qryHdr->num_rows();
            $getData = $qryHdr->row_array();
            // --------------------------------------
            if ($checkData > 0)
            {
                if ($status_code == 'RA010') // VOID DATA REGULATED
                {
                    $this->db->where(['waybill_smu' => $mawb]);
                    $Inactive = $this->db->update('th_regulated', ['_is_active' => 0]); 
                    if ($Inactive) 
                    {
                        $arr_result = ['s' => 'success', 'm' => "Mawb $mawb data is successfully turned OFF !"];
                        $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK  
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Void data is failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                }
                else
                {
                    // REGULATED AGENT
                    if ($status_code == 'RA001') {
                        $table = 'td_regulated_acceptance';
                    } elseif ($status_code == 'RA002') {
                        $table = 'td_regulated_screening';
                    } elseif ($status_code == 'RA003') {
                        $table = 'td_outbond_weighing';
                    } elseif ($status_code == 'RA004') {
                        $table = 'td_regulated_rejected';
                    } elseif ($status_code == 'RA005') {
                        $table = 'td_regulated_storage';
                    } elseif ($status_code == 'RA006') {
                        $table = 'td_regulated_csd';
                    } elseif ($status_code == 'RA007') {
                        $table = 'td_regulated_loading';
                    } elseif ($status_code == 'RA008') {
                        $table = 'td_regulated_transport';
                    } elseif ($status_code == 'RA009') {
                        $table = 'td_regulated_handover';
                    } else {
                        $table = '';
                    }
                    // --------------------------------------
                    
                    if ($status_code == 'RA004') // item rejected
                    {
                        if (empty($total_item) || empty($desc_item))
                        {
                            $this->response(['s' => 'fail', 'm' => 'Enter your parameters correctly (total_item & desc_item) !'], REST_Controller::HTTP_BAD_REQUEST); // 400
                            $this->output->_display(); // jika exit maka tambhakan baris ini, (untuk cetak msg)
                            exit;
                        } 
                        elseif (!is_numeric($total_item))
                        {
                            $this->response(['s' => 'fail', 'm' => 'The total item field must be a number !'], REST_Controller::HTTP_BAD_REQUEST); // 400
                            $this->output->_display(); // jika exit maka tambhakan baris ini, (untuk cetak msg)
                            exit;
                        }
                        else
                        {
                            $data = [
                                'id_header'     => $getData['id_'],
                                'status_date'   => $status_date,
                                'status_time'   => $status_time,
                                'total_item'    => $total_item,
                                'desc_item'     => $desc_item,
                                '_is_active'    => 1
                            ];
                        }
                    }
                    else
                    {
                        $data = [
                            'id_header'     => $getData['id_'],
                            'status_date'   => $status_date,
                            'status_time'   => $status_time,
                            '_is_active'    => 1
                        ];
                    }

                    $insert = $this->db->insert($table, $data);
                    if ($insert) 
                    {
                        $arr_result = ['s' => 'success', 'm' => 'Insert data successfully !'];
                        $this->response($arr_result, REST_Controller::HTTP_CREATED); // 201 Created
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Insert data failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                }  // End: check void
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Data not found !'];
                $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
            } // end: check data by mawb
        } // end: check validations
    }
}
