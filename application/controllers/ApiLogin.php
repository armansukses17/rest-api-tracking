<?php
// ------------------------------------------
// REST SERVER LOGIN
// Created: 05/01/2021
// Updated: -
// ------------------------------------------
//###########################################
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ApiLogin extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() 
    {
        $this->response(['Not found !'], REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
    }

    function login_post()
    {
        $email = $this->post('email');
        $password = $this->post('password');
        // --------------------------------------

        if ($email == '' || $password == '')
        {
            $arr_result = ['s' => 'fail', 'm'=>'Data cannot be empty !'];
            $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        else        
        {
            $this->db->select('*');
            $this->db->where(['email' => $email, 'is_active' => '1']);
            $query = $this->db->get('user');
            $checkData = $query->num_rows();
            $data = $query->row_array();
            // print_r($data); die;
            if ($checkData == 1)
            {
                // cek password nya
                if (password_verify($password, $data['password'])) 
                {
                    $data = [
                        'email' => $data['email'],
                        'role_id' => $data['role_id'],
                        'user_id' => $data['id'],
                        'user_nama' => $data['name'],
                        'user_email' => $data['email'],
                        'user_image' => $data['image'],
                        'user_date' => $data['date_created'],
                        'active_db' => $this->config->item('prefixdb'),
                        'access_domain' => $data['access_domain'] // 1 = MAU, 2 = RA
                    ];
                    // $this->session->set_userdata($data);
                    $arr_result = ['s' => 'success', 'm' => 'Login Success!', 'd' => $data];
                    $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK     
                } 
                else 
                {
                    $arr_result = ['s' => 'fail', 'm'=>'Wrong password!', 'd' => []];
                    $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                }
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Incorrect username or password!'];
                $this->response($arr_result, REST_Controller::HTTP_NOT_FOUND); // 404 Not Found  
            }
        }
    }
}
