<?php
// ------------------------------------------
// REST SERVER TRACKING RA
// Created: 17/01/2021
// Updated: -
// ------------------------------------------
//###########################################
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ApiTrackingRa extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() 
    {
        $this->response(['Not found !'], REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
    }
}
