<?php
// ------------------------------------------
// REST SERVER TRACKING MAU
// Created: 05/01/2021
// Updated: -
// ------------------------------------------
//###########################################
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class RestServerMau extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    function index_get() 
    {
        $this->response(['Not found !'], REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
    }

    // Search data by mawb
    function mawb_get() 
    {
        $string = $this->get('param'); // use param 
        // $string = $this->uri->segment(3); // by url (http://localhost/tracking/tracking-rest/RestTracking/mawb/8)
        if (!empty($string))
        {
            $this->db->select('id_, waybill_smu');
            $this->db->like('waybill_smu', $string);
            $this->db->where('_is_active', 1);
            $this->db->group_by('waybill_smu');
            $this->db->limit(10);
            $qryInbound = $this->db->get('th_inbound');
            $checkData1 = $qryInbound->num_rows();

            $this->db->select('id_, waybill_smu');
            $this->db->like('waybill_smu', $string);
            $this->db->where('_is_active', 1);
            $this->db->group_by('waybill_smu');
            $this->db->limit(10);
            $qryOutbond = $this->db->get('th_outbond');
            $checkData2 = $qryOutbond->num_rows();

            if ($checkData1 > 0) 
            {
                $result = $qryInbound->result();
                foreach ($result as $row)
                {
                    $data[] = ['waybill_smu' => $row->waybill_smu];
                }
                $arr_result = ['s' => 'success', 'd' => $data];
                $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK          
            }
            elseif ($checkData2 > 0) 
            {
                $result = $qryOutbond->result();
                foreach ($result as $row)
                {
                    $data[] = ['waybill_smu' => $row->waybill_smu];
                }
                $arr_result = ['s' => 'success', 'd' => $data];
                $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK          
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Not Found !', 'd' => []];
                $this->response($arr_result, REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
            }
        } 
        else 
        {
            $arr_result = ['s' => 'fail', 'm' => 'Bad request !', 'd' => []];
            $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        // echo json_encode($arr_result);
    }

    // after get and select mawb / waybill_smu, send this function by mawb / waybill_smu
    // get status by mawb (misal: th_inbound, th_outbond, atau th_regulated)
    // base_url()/status/99013695894
    function status_get() 
    {
        // $waybill_smu = $this->uri->segment(3);
        $waybill_smu = $this->get('mawb'); // use param 
        if (!empty($waybill_smu))
        {
            $this->db->select('*');
            $this->db->where(['waybill_smu' => $waybill_smu, '_is_active' => 1]);
            $query1 = $this->db->get('th_inbound');
            $checkInbound = $query1->num_rows();
            $getInbond = $query1->row_array();
            // ------------------------------------------
            $this->db->select('*');
            $this->db->where(['waybill_smu' => $waybill_smu, '_is_active' => 1]);
            $query2 = $this->db->get('th_outbond');
            $checkOutbond = $query2->num_rows();
            // $getOutbond = $query2->row_array();
            // ------------------------------------------
            if ($checkInbound > 0) 
            {
                if ($getInbond['gate_type'] == 'import' || $getInbond['gate_type'] == 'incoming') {
                    $clauseWhere = 'INBOUND';
                } 
                elseif ($getInbond['gate_type'] == 'transit') {
                    $clauseWhere = 'TRANSIT';
                } 
                else {
                    $clauseWhere = '';
                }
                $this->db->select('*');
                $this->db->where('proses_type', $clauseWhere);
                $this->db->order_by('_id', 'ASC');
                $queryStatus = $this->db->get('m_status_tracking');
                $checkStatus = $queryStatus->num_rows();
                $getData = $queryStatus->result();
                if ($checkStatus > 0) 
                {
                    foreach ($getData as $row) 
                    {
                        $data[] = [
                            'status_code' => $row->status_kode, 
                            'status_name' => $row->proses_type.' - '.$row->status_name
                        ];
                    }
                    $arr_result = ['s' => 'success', 'd' => $data];
                    $this->response($arr_result, REST_Controller::HTTP_OK);  // 200 OK
                }
            }
            elseif ($checkOutbond > 0) 
            {
                $this->db->select('*');
                $this->db->where('proses_type', 'OUTBOUND');
                $this->db->order_by('_id', 'ASC');
                $queryStatus = $this->db->get('m_status_tracking');
                $checkStatus = $queryStatus->num_rows();
                $getData = $queryStatus->result();
                if ($checkStatus > 0) 
                {
                    foreach ($getData as $row) 
                    {
                        $data[] = [
                            'status_code' => $row->status_kode, 
                            'status_name' => $row->proses_type.' - '.$row->status_name
                        ];
                    }
                    $arr_result = ['s' => 'success', 'd' => $data];
                    $this->response($arr_result, REST_Controller::HTTP_OK);  // 200 OK
                }
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Not Found !'];
                $this->response($arr_result, REST_Controller::HTTP_NOT_FOUND); // 404 Not Found
            }
        } 
        else 
        {
            $arr_result = ['s' => 'fail', 'm' => 'Bad request !'];
            $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
    }

    // post data 
    function create_post() 
    {
        // kirim parameter post yang dibutuhkan
        // ################################3###
        // $yourParam = [
        //     'mawb'          => 'xxxxx',
        //     'status_code'   => 'xxxxx',
        //     'date_time'     => 'YYYY-mm-dd H:I',
        // ];
        // --------------------------------
        $mawb = $this->post('mawb');
        $status_code = $this->post('status_code');
        $date_time = $this->post('date_time'); // format : YYYY-mm-dd H:I
        // --------------------------------------
        if (empty($mawb) || empty($status_code) || empty($date_time))
        {
            $this->response(['s' => 'fail', 'm' => 'Enter your parameters correctly !'], REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        elseif (strlen($date_time) != 16) // jml string YYYY-mm-dd H:I
        {
            $this->response(['s' => 'fail', 'm' => 'Wrong date fornmat !'], REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
        }
        else
        {
            $explodeDate = explode(' ', $date_time);
            $status_date = $explodeDate[0]; // date
            $status_time = $explodeDate[1]; // time

            $this->db->select('proses_type');
            $this->db->where('status_kode', $status_code);
            $qry = $this->db->get('m_status_tracking');
            $getDataStatus = $qry->row_array();

            if ($getDataStatus['proses_type'] == 'INBOUND' || $getDataStatus['proses_type'] == 'TRANSIT')
            {
                $tableHeader = 'th_inbound';
            }
            elseif ($getDataStatus['proses_type'] == 'OUTBOUND')
            {
                $tableHeader = 'th_outbond';
            }
            $this->db->select('id_, waybill_smu');
            $this->db->where('waybill_smu', $mawb);
            $qryHdr = $this->db->get($tableHeader);
            $checkData = $qryHdr->num_rows();
            $getData = $qryHdr->row_array();
            // --------------------------------------
            if ($checkData > 0)
            {
                if ($status_code == 'IN006') // VOID DATA INBOUND
                {
                    $this->db->where(['waybill_smu' => $mawb]);
                    $Inactive = $this->db->update('th_inbound', ['_is_active' => 0]); 
                    if ($Inactive) 
                    {
                        $arr_result = ['s' => 'success', 'm' => "Mawb $mawb data is successfully turned OFF !"];
                        $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK  
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Void data is failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                }
                elseif ($status_code == 'OU009') // VOID DATA OUTBOUND
                {
                    $this->db->where(['waybill_smu' => $mawb]);
                    $Inactive = $this->db->update('th_outbond', ['_is_active' => 0]); 
                    if ($Inactive) 
                    {
                        $arr_result = ['s' => 'success', 'm' => "Mawb $mawb data is successfully turned OFF !"];
                        $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK  
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Void data is failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                }
                elseif ($status_code == 'TR009') // VOID DATA TRANSIT
                {
                    $this->db->where(['waybill_smu' => $mawb]);
                    $Inactive = $this->db->update('th_inbound', ['_is_active' => 0]); 
                    if ($Inactive) 
                    {
                        $arr_result = ['s' => 'success', 'm' => "Mawb $mawb data is successfully turned OFF !"];
                        $this->response($arr_result, REST_Controller::HTTP_OK); // 200 OK  
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Void data is failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                } 
                else
                {
                    // INBOUND 1
                    if ($status_code == 'IN001') {
                        $table = 'td_inbound_delivery_aircarft';
                    } elseif ($status_code == 'IN002') {
                        $table = 'td_inbound_breakdown';
                    } elseif ($status_code == 'IN003') {
                        $table = 'td_inbound_storage';
                    } elseif ($status_code == 'IN004') {
                        $table = 'td_inbound_clearance';
                    } elseif ($status_code == 'IN005') {
                        $table = 'td_inbound_pod';
                    }
                    // INBOUND 2 (TRANSIT)
                    elseif ($status_code == 'TR001') {
                        $table = 'td_inbound_delivery_aircarft';
                    } elseif ($status_code == 'TR002') {
                        $table = 'td_inbound_breakdown';
                    } elseif ($status_code == 'TR003') {
                        $table = 'td_inbound_storage';
                    } elseif ($status_code == 'TR004') {
                        $table = 'td_inbound_transit';
                    } elseif ($status_code == 'TR005') {
                        $table = 'td_inbound_transit_buildup';
                    } elseif ($status_code == 'TR006') {
                        $table = 'td_inbound_transit_delivery_staging';
                    } elseif ($status_code == 'TR007') {
                        $table = 'td_inbound_delivery_aircarft';
                    } elseif ($status_code == 'TR008') {
                        $table = 'td_inbound_loading_aircarft';
                    } 
                    // OUTBOND
                    elseif ($status_code == 'OU001') {
                        $table = 'td_outbond_acceptance';
                    } elseif ($status_code == 'OU002') {
                        $table = 'td_outbond_weighing';
                    } elseif ($status_code == 'OU003') {
                        $table = 'td_outbond_manifest';
                    } elseif ($status_code == 'OU004') {
                        $table = 'td_outbond_storage';
                    } elseif ($status_code == 'OU005') {
                        $table = 'td_outbond_buildup';
                    } elseif ($status_code == 'OU006') {
                        $table = 'td_outbond_delivery_staging';
                    } elseif ($status_code == 'OU007') {
                        $table = 'td_outbond_delivery_aircarft';
                    } elseif ($status_code == 'OU008') {
                        $table = 'td_outbond_loading_aircarft';
                    } else {
                        $table = '';
                    }
                    // --------------------------------------
                    $data = [
                        'id_header'     => $getData['id_'],
                        'status_date'   => $status_date,
                        'status_time'   => $status_time,
                        '_is_active'     => 1
                    ];

                    $insert = $this->db->insert($table, $data);
                    if ($insert) 
                    {
                        $arr_result = ['s' => 'success', 'm' => 'Insert data successfully !'];
                        $this->response($arr_result, REST_Controller::HTTP_CREATED); // 201 Created
                    } 
                    else 
                    {
                        $arr_result = ['s' => 'fail', 'm' => 'Insert data failed !'];
                        $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
                    }
                } // End: check void
            }
            else
            {
                $arr_result = ['s' => 'fail', 'm' => 'Data not found !'];
                $this->response($arr_result, REST_Controller::HTTP_BAD_REQUEST); // 400 Bad Request
            } // end: check data by mawb
        } // end: check validations
    }
}
