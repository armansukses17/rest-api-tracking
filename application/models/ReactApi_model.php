<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReactApi_model extends CI_model {

  public function __construct()
  {
        parent::__construct();
        // $this->db       = $this->load->database('r_dbmaster', TRUE); // db master
        // $this->r_dbpeb  = $this->load->database('r_dbpeb', TRUE); // db ekspor (read)
        // $this->w_dbpeb  = $this->load->database('w_dbpeb', TRUE); // db ekspor (create, update, delete)
        date_default_timezone_set('Asia/Jakarta');
  }

  
  public function get_products()
  {
    $this->db->where('is_active', 1);
    $query = $this->db->get('products');
    return $query->result();
  }

  public function get_product($productId)
  {
    $this->db->where('is_active', 1);
    $this->db->where('id', $productId);
    $query = $this->db->get('products');
    return $query->row();
  }

  public function insert_product($productData)
  {
    return $this->db->insert('products', $productData);
    // return $this->db->insert_id();
  }

  public function update_product($id, $productData)
  {
    $this->db->where('id', $id);
    $this->db->update('products', $productData);
  }

  public function delete_product($productId)
  {
    $this->db->where('id', $productId);
    $this->db->delete('products');
  }
}
